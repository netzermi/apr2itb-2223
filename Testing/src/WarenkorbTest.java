import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.*;
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WarenkorbTest {
    Produkt p1, p2;
    Bestellposition pb1, pb2;
    Warenkorb warenkorb;
    @BeforeAll
    void init(){
         p1 = new Produkt("PS5", 450d);
         p2 = new Produkt("GOW", 75d);
         pb1 = new Bestellposition(p1, 1);
         pb2 = new Bestellposition(p2, 2);
         warenkorb = new Warenkorb();
        warenkorb.bestellPositionHinzufügen(pb1);
        warenkorb.bestellPositionHinzufügen(pb2);
    }


    @org.junit.jupiter.api.Test
    void berechneSumme() {
        assertEquals(600d,warenkorb.berechneSumme(),1E-5);
    }
    @Test
    void getMenge(){
        assertEquals(1, pb1.getMenge());

    }
}