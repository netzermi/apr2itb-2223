import java.util.ArrayList;
import java.util.List;

public class Warenkorb {
    List<Bestellposition> bestellpositionList;

    public Warenkorb(){
        this.bestellpositionList = new ArrayList<>();
    }

    public void bestellPositionHinzufügen(Bestellposition b){
        bestellpositionList.add(b);
    }

    public double berechneSumme(){
        double summe = 0;
        for(Bestellposition bestellposition : bestellpositionList){
           Produkt produkt = bestellposition.getProdukt();
           int menge = bestellposition.getMenge();
           double preis = produkt.getPrice();
           summe += preis*menge;
        }
        return summe;
    }



}
