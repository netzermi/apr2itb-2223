public class App2 {

    public static void main(String[] args) {
        Produkt p1 = new Produkt("PS5", 450d);
        Produkt p2 = new Produkt("GOW", 75d);
        Bestellposition pb1 = new Bestellposition(p1, 1);
        Bestellposition pb2 = new Bestellposition(p2, 2);
        Warenkorb warenkorb = new Warenkorb();
        warenkorb.bestellPositionHinzufügen(pb1);
        warenkorb.bestellPositionHinzufügen(pb2);
        System.out.println(warenkorb.berechneSumme());


    }
}
