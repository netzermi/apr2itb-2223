package at.hakimst.apr;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;

public class ListComp {
    public static void main(String[] args) {
        int[] randomNumbers = getRandomIndexes(1000);
        System.out.println(Arrays.toString(randomNumbers));

        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer>  linkedList = new LinkedList<>();
        for(int i : randomNumbers){
            arrayList.add(i);
            linkedList.add(i);
        }

        long start = System.currentTimeMillis();
        for(int i : randomNumbers){
            arrayList.add(i, 3);
        }
        System.out.println("ArrayList:" + (System.currentTimeMillis() - start));

        start = System.currentTimeMillis();
        for(int i : randomNumbers){
            linkedList.add(i, 3);
        }
        System.out.println("LinkedList:" + (System.currentTimeMillis() - start));

    }

    public static int[] getRandomIndexes(int n){
        int[] indexes = new int[n];
        Random rand = new Random(1);
        for(int i = 0; i < n; i++){
            indexes[i] = rand.nextInt(n);
        }
        return indexes;
    }
}
