package at.hakimst.apr;



import java.util.ArrayList;
import java.util.Iterator;

public class App {

    public static void main(String[] args) {

        ArrayList<String> liste = new ArrayList<>();
        liste.add("Max");
        liste.add("Susi");
        liste.add("Andrea");

        Iterator<String> iterator = liste.iterator();
        while (iterator.hasNext()){
            String name = iterator.next();
            if(name.equals("Max")){
                iterator.remove();
            }
        }
        liste.forEach(System.out::println);

//        System.out.println(liste.size());
//        //Variante 1 mit for-Schleife
//        System.out.println("**For-Schleife**");
//        for (int i = 0; i < liste.size(); i++) {
//            System.out.println(liste.get(i));
//        }
//        //Variante 2 mit for-each
//        System.out.println("**For-each**");
//        for (String s : liste) {
//            System.out.println(s);
//        }
//        //Variante 3 mit Lambda
//        System.out.println("**Lambda**");
//        liste.forEach(System.out::println);
    }


}
