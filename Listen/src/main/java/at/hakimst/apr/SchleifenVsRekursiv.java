package at.hakimst.apr;

public class SchleifenVsRekursiv {

    public static void main(String[] args) {
        System.out.println(fakultaet2(3));
    }

    public static long fakultaet(int n){
        long faktultaet = 1;
        for(int i = 1; i <= n; i++){
            faktultaet = faktultaet * i;
        }
        return faktultaet;
    }

    public static long fakultaet2(int n){
        if(n == 1){
            return 1;
        } else {
            return n* fakultaet2(n-1);
        }
    }


}
