import javax.swing.*;
import java.util.Scanner;

public class DataTypes {

    public static void main(String[] args) {
//        double x = 3d/4d;
//        System.out.println(x);

        int i = 130;
        byte b = (byte) i;
        System.out.println(b);

        float f = 3.4f;

        String s = "123";
        int si = Integer.parseInt(s);

        JOptionPane.showMessageDialog(null, "Hallo Welt");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.next();
        System.out.println("Hallo " + name);
    }
}
