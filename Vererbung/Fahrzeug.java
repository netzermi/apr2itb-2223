public class Fahrzeug {
    private String name;

    public Fahrzeug(String name){
        this.name = name;
    }



    public void fahren(){
        System.out.println("Fahrzeug " + name + " fährt");
    }


}
