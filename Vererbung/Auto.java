public class Auto extends Fahrzeug{
    private boolean spoiler;
    public Auto(String name, boolean spoiler){
        super(name);
        this.spoiler = spoiler;

    }

    @Override
    public void fahren(){
        System.out.println("Das Auto fährt");
    }



}
