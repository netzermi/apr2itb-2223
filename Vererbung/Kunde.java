import java.util.Objects;

public class Kunde extends Object{
    private int kn;
    private String name;

    public Kunde(int kn, String name){
        this.kn = kn;
        this.name = name;
    }

    @Override
    public String toString(){
        return super.toString() + " " + kn + " " + name;
    }

    //k1.equals(k2) -> this = k1, o = k2
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Kunde kunde = (Kunde) o;
        return (this.kn == kunde.kn && Objects.equals(name, kunde.name));
    }

    @Override
    public int hashCode() {
        return Objects.hash(kn, name);
    }
}
