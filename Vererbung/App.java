import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        Auto bmw = new Auto("BMW X3", true);
        Motorrad honda = new Motorrad("Honda Hornet");
        bmw.fahren();
        honda.fahren();

        Fahrzeug f1 = new Fahrzeug("Skoda Kodiaq");
        Fahrzeug f2 = new Auto("BMW X1", false);
        Fahrzeug f3 = new Motorrad("Kawasaki");
        Auto f4 = new Auto("Toyota", false);
        //Auto f5 = new Fahrzeug("Tesla S");
        List<Fahrzeug> fahrzeugListe = new ArrayList<>();
        fahrzeugListe.add(f1);
        fahrzeugListe.add(f2);
        fahrzeugListe.add(f3);
        fahrzeugListe.add(f4);
        for(Fahrzeug f : fahrzeugListe){
            f.fahren();
        }

    }
}
